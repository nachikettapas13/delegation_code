// Allows us to use ES6 in our migrations and tests.
require('babel-register')
var HDWalletProvider = require("truffle-hdwallet-provider");

var mnemonic = "animal inner drip harsh salad warrior select athlete chapter dish deposit sweet";

module.exports = {
  networks: {
    development: {
      host: '127.0.0.1',
      port: 8545,
      network_id: '*', // Match any network id
      from: '0xD7ee7199C49c96239e20BA52A16F6bd7735b4dbD'
    },

    ropsten:  {
      network_id: 3,
      provider: function() { 
        return new HDWalletProvider(mnemonic, "https://ropsten.infura.io/ReKywYBY4fVkjdIllT4b");
      },
      gas: 450000,
      gasPrice: 25000000000
    },

    kovan: {
      provider: function() { 
        return new HDWalletProvider(mnemonic, 'https://kovan.infura.io/ReKywYBY4fVkjdIllT4b');
      },
      network_id: '42',
      gas: 5000000,
      gasPrice: 25000000000
    },

    rinkeby: {
      network_id: 4,
      provider: function() { 
        return new HDWalletProvider(mnemonic, 'https://rinkeby.infura.io/ReKywYBY4fVkjdIllT4b');
      },
      gas: 5000000,
      gasPrice: 25000000000
    }
  }
}
