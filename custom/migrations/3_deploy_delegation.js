/*var Delegation = artifacts.require("./Delegation.sol");
var Role = artifacts.require("./Role.sol");
var fs = require('fs')
const {performance} = require('perf_hooks');
var t0,t1;

var logger = fs.createWriteStream('Delegation_Deployment_Time_Ropsten.txt', {
  flags: 'a' // 'a' means appending (old data will be preserved)
})

module.exports = function(deployer) {
  // Use deployer to state migration tasks.
  deployer.then(function() {
    return Role.deployed();
  }).then(function(instance) {
      console.log("This is the role's address: " + instance.address);
      logger.write("Performance output for run on " + new Date());
      logger.write("\n");
      t0 = performance.now();
      return deployer.deploy(Delegation, instance.address);
  }).then(function() {
    t1 = performance.now();
    logger.write("Start time: " + t0 + ", End time: " + t1 + ", Total time (in sec): " + Number(((t1 - t0)/1000).toFixed(3)));
    logger.write("\n");
  });
};*/

var Delegation = artifacts.require("./Delegation.sol");
var Role = artifacts.require("./Role.sol");

module.exports = function(deployer) {
  deployer.then(function() {
    return Role.deployed();
  }).then(function(instance) {
      return deployer.deploy(Delegation, instance.address);
  });
};