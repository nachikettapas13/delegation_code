/*var Migrations = artifacts.require("./Migrations.sol");
var Role = artifacts.require("./Role.sol");
var fs = require('fs')
const {performance} = require('perf_hooks');
var t0,t1;

var logger = fs.createWriteStream('Role_Deployment_Time_Ropsten.txt', {
  flags: 'a' // 'a' means appending (old data will be preserved)
})

module.exports = function(deployer) {
  deployer.then(function() {
    return Migrations.deployed();
  }).then(function(instance) {
      console.log("This is the migration's address: " + instance.address);
      logger.write("Performance output for run on " + new Date());
      logger.write("\n");
      t0 = performance.now();
      return deployer.deploy(Role);
  }).then(function() {
    t1 = performance.now();
    logger.write("Start time: " + t0 + ", End time: " + t1 + ", Total time (in sec): " + Number(((t1 - t0)/1000).toFixed(3)));
    logger.write("\n");
  });
};*/
var Migrations = artifacts.require("./Migrations.sol");
var Role = artifacts.require("./Role.sol");

module.exports = function(deployer) {
  deployer.then(function() {
    return Migrations.deployed();
  }).then(function(instance) {
      return deployer.deploy(Role);
  });
};
