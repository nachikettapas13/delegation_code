// Import the page's CSS. Webpack will know what to do with it.
import "../stylesheets/app.css";

// Import libraries we need.
import { default as Web3 } from 'web3';
import { default as contract } from 'truffle-contract';

// Import our contract artifacts and turn them into usable abstractions.
import delegation_artifacts from '../../build/contracts/Delegation.json';
import role_artifacts from '../../build/contracts/Role.json';
//import combinedcode_artifacts from '../../build/contracts/CombinedCode.json';

//import data from '../../migrations/input.csv';

var parse = require('csv-parse');
var async = require('async');

var t0, t1;

// MetaCoin is our usable abstraction, which we'll use through the code below.
var Delegation = contract(delegation_artifacts);
var Role = contract(role_artifacts);

// The following code is simple to show off interacting with your contracts.
// As your needs grow you will likely need to change its form and structure.
// For application bootstrapping, check out window.addEventListener below.
var accounts;
var account;

window.App = {
  start: function () {
    var self = this;

    // Bootstrap the Delegation abstraction for Use.
    Delegation.setProvider(web3.currentProvider);
    Role.setProvider(web3.currentProvider);

    if (typeof Delegation.currentProvider.sendAsync !== "function") {
      Delegation.currentProvider.sendAsync = function () {
        return Delegation.currentProvider.send.apply(
          Delegation.currentProvider, arguments
        );
      };
    }

    if (typeof Role.currentProvider.sendAsync !== "function") {
      Role.currentProvider.sendAsync = function () {
        return Role.currentProvider.send.apply(
          Role.currentProvider, arguments
        );
      };
    }

    // Get the initial account balance so it can be displayed.
    web3.eth.getAccounts(function (err, accs) {
      if (err != null) {
        alert("There was an error fetching your accounts.");
        return;
      }

      if (accs.length == 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
        return;
      }

      accounts = accs;
      account = accounts[0];
    });

    self.populatePermission("divRoles");
    //self.automatedTesting();
  },

  automatedTesting: function () {
    //Code for simulators
    var txtFile = new XMLHttpRequest();
    txtFile.onload = function () {
      var allText = txtFile.responseText;
      var allTextLines = allText.split(/\r\n|\n/);

      for (var i = 0; i < allTextLines.length; i++) {
        var allValues = allTextLines[i].split(',');

        var deleg;
        Delegation.deployed().then(function (instance) {
          deleg = instance;
          t0 = new Date().getTime();
          return deleg.isUserAllowed.call(allValues[0], allValues[1], allValues[2]);
        }).then(function (result) {
          if (result.valueOf() == true) {
            t1 = new Date().getTime();
            console.log("Start time: " + t0 + ", End time: " + t1 + ", Total time (in sec): " + Number(((t1 - t0) / 1000).toFixed(3)));
            console.log("\n");
          }
        }).catch(function (e) {
          console.log(e);
          alert('Some error occurred!!');
        });
      }
    }
    txtFile.open("get", "../../migrations/input.csv", true);
    txtFile.send();

    //Code for public testnets
    /*var txtFile = new XMLHttpRequest();
    txtFile.onload = function () {
      var allText = txtFile.responseText;
      var allTextLines = allText.split(/\r\n|\n/);
   
      for (var i = 0; i < allTextLines.length; i++) {
        var allValues = allTextLines[i].split(',');
   
        var deleg;
        Delegation.deployed().then(function (instance) {
          deleg = instance;
          console.log("Performance output for run on " + new Date());
          console.log("\n");
          t0 = Math.floor(new Date().getTime() / 1000);
          deleg.addDelegation("", allValues[0], allValues[1], allValues[2], { from: account, gas: 500000 }).then(function (txHash) {
            console.log(t0);
            console.log(txHash.tx);
          }).catch(function (e) {
            console.log(e);
            alert('Some error occurred!!');
          });
        }).catch(function (e) {
          console.log(e);
          alert('Some error occurred!!');
        });
      }
    }
   
    txtFile.open("get", "../../migrations/input.csv", true);
    txtFile.send();*/

    /*async function waitForTxToBeMined (txHash) {
      let txReceipt
      while (!txReceipt) {
        try {
          alert(txHash);
          txReceipt = await web3.eth.getTransactionReceipt(txHash);
          //t1 = new Date().getTime();
          //console.log("Start time: " + t0 + ", End time: " + t1 + ", Total time (in sec): " + Number(((t1 - t0) / 1000).toFixed(3)));
          //console.log("\n");
          console.log("Completed!!")
        } catch (err) {
          console.log(err);
          return indicateFailure(err)
        }
      }
      indicateSuccess();
    }*/

    /*var txtFile = new XMLHttpRequest();
     txtFile.onload = function () {
       var allText = txtFile.responseText;
       var allTextLines = allText.split(/\r\n|\n/);
   
       for (var i = 0; i < allTextLines.length; i++) {
         web3.eth.getTransactionReceipt(allTextLines[i]).then(function (result) {
           web3.eth.getBlock(result.blockNumber).then(function (output) {
             console.log(output.transactions[0]);
             console.log(output.timestamp);
           })
         })
       }
     }
   
     txtFile.open("get", "../../migrations/txHash.csv", true);
     txtFile.send();*/

  },



  populatePermission: function (element) {
    var s2 = document.getElementById(element);
    s2.innerHTML = "";
    var optionArray = ["Create a new node", "Delete an existing node", "Show infromation about a node",
      "Create an SSH tunnel to a node", "Read a value from a sensor on a node", "Write a value to an actuator on a node",
      "Configure a node", "Inject code in a node"];
    var valueArray = ["Create", "Delete", "Show", "SSH", "Read", "Write", "Configure", "Inject"];

    for (var option in optionArray) {
      if (optionArray.hasOwnProperty(option)) {
        var pair = optionArray[option];
        var checkbox = document.createElement("input");
        checkbox.type = "checkbox";
        checkbox.name = "permValue[]";
        checkbox.value = valueArray[option];
        s2.appendChild(checkbox);

        var label = document.createElement('label')
        label.htmlFor = pair;
        label.appendChild(document.createTextNode(pair));

        s2.appendChild(label);
        s2.appendChild(document.createElement("br"));
      }
    }
  },

  /*populateRole: function () {
    var s2 = document.getElementById("divUserRoles");
    s2.innerHTML = "";
    var optionArray = new Array();
   
    var role;
    Role.deployed().then(function (instance) {
      role = instance;
      return role.getRoles.call();
    }).then(function (value) {
      if (value.valueOf() != "") {
        optionArray = value.valueOf();
   
        for (var option in optionArray) {
          if (optionArray.hasOwnProperty(option)) {
            var pair = web3.utils.toAscii(optionArray[option]).replace(/\u0000/g, '');
   
            var checkbox = document.createElement("input");
            checkbox.type = "radio";
            checkbox.name = "permValueRole[]";
            checkbox.value = pair;
            s2.appendChild(checkbox);
   
            var label = document.createElement('label')
            label.htmlFor = pair;
            label.appendChild(document.createTextNode(pair));
   
            s2.appendChild(label);
            s2.appendChild(document.createElement("br"));
          }
        }
      }
      else {
        alert('No Roles to display!!');
      }
    }).catch(function (e) {
      console.log(e);
      alert('Some error occurred!!');
    });
  },*/

  addRoleToChain: function () {
    var self = this;
    var roleName = document.getElementById('txtRole').value;
    var chk_arr = document.getElementsByName('permValue[]');
    var chklength = chk_arr.length;
    var permValue = new Array();

    for (var k = 0; k < chklength; k++) {
      if (chk_arr[k].checked) {
        permValue.push(chk_arr[k].value);
      }
    }

    var role;
    Role.deployed().then(function (instance) {
      role = instance;
      return role.checkRole.call(roleName);
    }).then(function (value) {
      if (value.valueOf() == false) {
        role.addRole(roleName, permValue, { from: account, gas: 5000000 });
      } else {
        return Promise.reject('102');
      }
      return role.checkRole.call(roleName);
    }).then(function (result) {
      if (result.valueOf() == true) {
        alert('Role added successfully!!');
      }
      else {
        alert('Not able to add role. Try Again!!');
      }
    }).catch(function (e) {
      if (e == '102') {
        alert('Role already exists!!');
      } else {
        console.log(e);
        alert('Some error occurred!!');
      }
    });

    //Reset code
    document.getElementById('txtRole').value = "";
    self.populatePermission("divRoles");
  },

  viewRole: function () {
    var self = this;
    var roleName = document.getElementById('txtRoleDisp').value;

    var role;
    Role.deployed().then(function (instance) {
      role = instance;
      return role.getRoleInfo.call(roleName);
    }).then(function (value) {
      if (value.valueOf() != "") {
        var optionArray = new Array();
        optionArray = value.valueOf();
        var ele = document.getElementById("divRolesDisp");
        ele.innerHTML = "";
        for (var option in optionArray) {
          if (optionArray.hasOwnProperty(option)) {
            //Replace option to to eliminate trailing zeros from bytes32 return value
            var pair = web3.utils.toAscii(optionArray[option]).replace(/\u0000/g, '');

            var label = document.createElement('label')
            label.htmlFor = pair;
            label.appendChild(document.createTextNode(pair));

            ele.appendChild(label);
            ele.appendChild(document.createElement("br"));
          }
        }
      }
      else {
        alert('Role does not exist!!');
      }
    }).catch(function (e) {
      console.log(e);
      alert('Some error occurred!!');
    });
  },

  deleteRoleFromChain: function () {
    var self = this;
    var roleName = document.getElementById('txtRoleDelete').value;

    var role;
    Role.deployed().then(function (instance) {
      role = instance;
      return role.checkRole.call(roleName);
    }).then(function (value) {
      if (value.valueOf() == true) {
        role.deleteRole(roleName, { from: account, gas: 500000 });
      } else {
        return Promise.reject('101');
      }
      return role.checkRole.call(roleName);
    }).then(function (result) {
      if (result.valueOf() == false) {
        alert('Role deleted successfully!!');
      }
      else {
        alert('Not able to delete role. Try Again!!');
      }
    }).catch(function (e) {
      if (e == '101') {
        alert('Role dosen\'t exists!!');
      } else {
        console.log(e);
        alert('Some error occurred!!');
      }
    });

    //Reset code
    document.getElementById('txtRoleDelete').value = "";
  },

  populatePermissionModify: function (element) {
    var self = this;
    var roleName = document.getElementById('txtRoleModify').value;

    if (roleName != "") {
      var s2 = document.getElementById(element);
      s2.innerHTML = "";
      var optionArray = ["Create a new node", "Delete an existing node", "Show infromation about a node",
        "Create an SSH tunnel to a node", "Read a value from a sensor on a node", "Write a value to an actuator on a node",
        "Configure a node", "Inject code in a node"];
      var valueArray = ["Create", "Delete", "Show", "SSH", "Read", "Write", "Configure", "Inject"];

      var role;
      Role.deployed().then(function (instance) {
        role = instance;
        return role.getRoleInfo.call(roleName);
      }).then(function (value) {
        if (value.valueOf() != "") {
          var checkedArray = new Array();
          checkedArray = value.valueOf();
          for (var option in optionArray) {
            if (optionArray.hasOwnProperty(option)) {
              var pair = optionArray[option];
              var checkbox = document.createElement("input");
              checkbox.type = "checkbox";
              checkbox.name = "permValueModify[]";
              checkbox.value = valueArray[option];
              for (var chk in checkedArray) {
                if (valueArray[option] == web3.utils.toAscii(checkedArray[chk]).replace(/\u0000/g, '')) {
                  checkbox.checked = true;
                }
              }
              s2.appendChild(checkbox);

              var label = document.createElement('label')
              label.htmlFor = pair;
              label.appendChild(document.createTextNode(pair));

              s2.appendChild(label);
              s2.appendChild(document.createElement("br"));
            }
          }
        } else {
          alert('Role does not exist!!');
        }
      }).catch(function (e) {
        console.log(e);
        alert('Some error occurred!!');
      });
    }
  },

  modifyRoleToChain: function () {
    var self = this;
    var roleName = document.getElementById('txtRoleModify').value;
    var chk_arr = document.getElementsByName('permValueModify[]');
    var chklength = chk_arr.length;
    var permValue = new Array();

    for (var k = 0; k < chklength; k++) {
      if (chk_arr[k].checked) {
        permValue.push(chk_arr[k].value);
      }
    }

    var role;
    Role.deployed().then(function (instance) {
      role = instance;
      role.modifyRole(roleName, permValue, { from: account, gas: 500000 });
      return role.checkRole.call(roleName);
    }).then(function (result) {
      if (result.valueOf() == true) {
        alert('Role modified successfully!!');
      }
      else {
        alert('Not able to modify role. Try Again!!');
      }
    }).catch(function (e) {
      console.log(e);
      alert('Some error occurred!!');
    });

    //Reset code
    document.getElementById('txtRoleModify').value = "";
    document.getElementById('divRolesModify').innerHTML = "";
  },

  getRolesFromChain: function () {
    var role;
    Role.deployed().then(function (instance) {
      role = instance;
      return role.getRoles.call();
    }).then(function (value) {
      if (value.valueOf() != "") {
        var optionArray = new Array();
        optionArray = value.valueOf();
        var ele = document.getElementById("divAllRolesDisp");
        ele.innerHTML = "";
        for (var option in optionArray) {
          if (optionArray.hasOwnProperty(option)) {
            //Replace option to to eliminate trailing zeros from bytes32 return value
            var pair = web3.utils.toAscii(optionArray[option]).replace(/\u0000/g, '');

            var label = document.createElement('label')
            label.htmlFor = pair;
            label.appendChild(document.createTextNode(pair));

            ele.appendChild(label);
            ele.appendChild(document.createElement("br"));
          }
        }
      }
      else {
        alert('No Roles to display!!');
      }
    }).catch(function (e) {
      console.log(e);
      alert('Some error occurred!!');
    });
  },

  //User fucntions to be changed to Delegation
  addDelegationToChain: function () {
    var self = this;
    var userId = document.getElementById('txtUserIDAdd').value;
    var nodeId = document.getElementById('txtNodeIDAdd').value;
    var delegId = document.getElementById('txtDelegIDAdd').value;
    var roleAssign = document.getElementById('txtRoleDelegAdd').value;

    var role;
    Role.deployed().then(function (instance) {
      role = instance;
      return role.checkRole.call(roleAssign);
    }).then(function (value) {
      if (value.valueOf() == true) {

        var deleg;
        Delegation.deployed().then(function (instance) {
          deleg = instance;
          return deleg.checkDelegation.call(userId, nodeId, roleAssign);
        }).then(function (value) {
          if (value.valueOf() == false) {
            deleg.addDelegation(delegId, userId, nodeId, roleAssign, { from: account, gas: 500000 });
          } else {
            return Promise.reject('103');
          }
          return deleg.checkDelegation.call(userId, nodeId, roleAssign);
        }).then(function (result) {
          if (result.valueOf() == true) {
            alert('Delegation added successfully!!');
          }
          else {
            alert('Not able to add delegation. Try Again!!');
          }
        }).catch(function (e) {
          if (e == '103') {
            alert('Delegation already exists!!');
          } else {
            console.log(e);
            alert('Some error occurred!!');
          }
        });

      } else {
        return Promise.reject('101');
      }
    }).catch(function (e) {
      if (e == '101') {
        alert('Role dosen\'t exists!!');
      } else {
        console.log(e);
        alert('Some error occurred!!');
      }
    });

    //Reset code
    document.getElementById('txtUserIDAdd').value = "";
    document.getElementById('txtNodeIDAdd').value = "";
    document.getElementById('txtDelegIDAdd').value = "";
    document.getElementById('txtRoleDelegAdd').value = "";
  },

  deleteDelegationFromChain: function () {
    var self = this;
    var userId = document.getElementById('txtUserIDDelete').value;
    var nodeId = document.getElementById('txtNodeIDDelete').value;
    var roleAssign = document.getElementById('txtRoleDelegDelete').value;

    var role;
    Role.deployed().then(function (instance) {
      role = instance;
      return role.checkRole.call(roleAssign);
    }).then(function (value) {
      if (value.valueOf() == true) {

        var deleg;
        Delegation.deployed().then(function (instance) {
          deleg = instance;
          return deleg.checkDelegation.call(userId, nodeId, roleAssign);
        }).then(function (value) {
          if (value.valueOf() == true) {
            deleg.deleteDelegation(userId, nodeId, roleAssign, { from: account, gas: 500000 });
          } else {
            return Promise.reject('104');
          }
          return deleg.checkDelegation.call(userId, nodeId, roleAssign);
        }).then(function (result) {
          if (result.valueOf() == false) {
            alert('Delegation deleted successfully!!');
          }
          else {
            alert('Not able to delete delegation. Try Again!!');
          }
        }).catch(function (e) {
          if (e == '104') {
            alert('Delegation does not exist!!');
          } else {
            console.log(e);
            alert('Some error occurred!!');
          }
        });

      } else {
        return Promise.reject('101');
      }
    }).catch(function (e) {
      if (e == '101') {
        alert('Role dosen\'t exists!!');
      } else {
        console.log(e);
        alert('Some error occurred!!');
      }
    });

    //Reset code
    document.getElementById('txtUserIDDelete').value = "";
    document.getElementById('txtNodeIDDelete').value = "";
    document.getElementById('txtRoleDelegDelete').value = "";
  },

  checkUserPermission: function () {
    var self = this;
    var userId = document.getElementById('txtUserIDCheck').value;
    var nodeId = document.getElementById('txtNodeIDCheck').value;
    var perm = document.getElementById('txtUserPermCheck').value;

    var deleg;
    Delegation.deployed().then(function (instance) {
      deleg = instance;
      return deleg.isUserAllowed.call(userId, nodeId, perm);
    }).then(function (value) {
      if (value.valueOf() == true) {
        document.getElementById('txtUserPermCheckResult').value = "True";
      } else {
        document.getElementById('txtUserPermCheckResult').value = "False";
      }
    }).catch(function (e) {
      if (e == "111") {
        alert('Delegation dosen\'t exists!!');
      } else {
        console.log(e);
        alert('Some error occurred!!');
      }
    });

    //Reset UI
    document.getElementById('txtUserIDCheck').value = "";
    document.getElementById('txtNodeIDCheck').value = "";
    document.getElementById('txtUserPermCheck').value = "";
  },

  checkDelegationInfo: function () {
    var self = this;
    var userId = document.getElementById('txtUserIDInfo').value;
    var nodeId = document.getElementById('txtNodeIDInfo').value;
    var role = document.getElementById('txtRoleDelegInfo').value;

    var deleg;
    Delegation.deployed().then(function (instance) {
      deleg = instance;
      return deleg.checkDelegation.call(userId, nodeId, role);
    }).then(function (value) {
      if (value.valueOf() == true) {
        return deleg.getDelegationInfo.call(userId, nodeId, role);
      } else {
        return Promise.reject('104');
      }
    }).then(function (value) {
      let delegId = value.valueOf();
      document.getElementById('txtDelegatorInfo').value = web3.utils.toAscii(delegId).replace(/\u0000/g, '');
    }).catch(function (e) {
      if (e == '104') {
        alert('Delegation does not exist!!');
      } else {
        console.log(e);
        alert('Some error occurred!!');
      }
    });

    //Reset UI
    document.getElementById('txtUserIDInfo').value = "";
    document.getElementById('txtNodeIDInfo').value = "";
    document.getElementById('txtRoleDelegInfo').value = "";
  },


  /* 
  populateUserModify: function () {
    var self = this;
    var userAddress = document.getElementById('txtUserModify').value;
 
    if (userAddress != "") {
      var s2 = document.getElementById('divUserRolesModify');
      s2.innerHTML = "";
 
      var deleg;
      Delegation.deployed().then(function (instance) {
        deleg = instance;
        return deleg.getRoles.call();
      }).then(function (value) {
        if (value.valueOf() != "") {
          var valueArray = new Array();
          valueArray = value.valueOf();
 
          var user;
          Users.deployed().then(function (instance) {
            user = instance;
            return user.getUserInfo.call(userAddress);
          }).then(function (value) {
            let [optionArray1,] = value.valueOf();
            if (optionArray1.length != 0) {
              for (var option in valueArray) {
                if (valueArray.hasOwnProperty(option)) {
                  //Replace option to to eliminate trailing zeros from bytes32 return value
                  var pair = web3.utils.toAscii(valueArray[option]).replace(/\u0000/g, '');
 
                  var checkbox = document.createElement("input");
                  checkbox.type = "checkbox";
                  checkbox.name = "permValueUserModify[]";
                  checkbox.value = pair;
                  for (var chk in optionArray1) {
                    if (web3.utils.toAscii(valueArray[option]).replace(/\u0000/g, '') == web3.utils.toAscii(optionArray1[chk]).replace(/\u0000/g, '')) {
                      checkbox.checked = true;
                    }
                  }
                  s2.appendChild(checkbox);
 
                  var label = document.createElement('label')
                  label.htmlFor = pair;
                  label.appendChild(document.createTextNode(pair));
 
                  s2.appendChild(label);
                  s2.appendChild(document.createElement("br"));
                }
              }
            }
            else {
              alert('User does not exist!!');
            }
          }).catch(function (e) {
            console.log(e);
            alert('Some error occurred!!');
          });
        }
        else {
          alert('No Roles to display!!');
        }
      }).catch(function (e) {
        console.log(e);
        alert('Some error occurred!!');
      });
    }
  },
 
  modifyUserOnChain: function () {
    var self = this;
    var userAddress = document.getElementById('txtUserModify').value;
    var chk_arr = document.getElementsByName('permValueUserModify[]');
    var chklength = chk_arr.length;
    var permValue = new Array();
 
    for (var k = 0; k < chklength; k++) {
      if (chk_arr[k].checked) {
        permValue.push(chk_arr[k].value);
      }
    }
 
    var user;
    Users.deployed().then(function (instance) {
      user = instance;
      user.modifyUser(userAddress, permValue, { from: account, gas: 500000 });
      return user.checkUser.call(userAddress);
    }).then(function (result) {
      if (result.valueOf() == true) {
        alert('User modified successfully!!');
      }
      else {
        alert('Not able to modify user. Try Again!!');
      }
    }).catch(function (e) {
      console.log(e);
      alert('Some error occurred!!');
    });
 
    //Reset code
    document.getElementById('txtUserModify').value = "";
    document.getElementById('divUserRolesModify').innerHTML = "";
  },
 
  getUserInformation: function () {
    var self = this;
    var userAddress = document.getElementById('txtUserInformation').value;
 
    var user;
    Users.deployed().then(function (instance) {
      user = instance;
      return user.getUserInfo.call(userAddress);
    }).then(function (value) {
      let [optionArray1, optionArray2] = value.valueOf();
      if (optionArray1.length != 0 || optionArray2.length != 0) {
        var ele = document.getElementById("divUserRolesAssigned");
        ele.innerHTML = "";
        for (var option in optionArray1) {
          if (optionArray1.hasOwnProperty(option)) {
            //Replace option to to eliminate trailing zeros from bytes32 return value
            var pair = web3.utils.toAscii(optionArray1[option]).replace(/\u0000/g, '');
 
            var label = document.createElement('label')
            label.htmlFor = pair;
            label.appendChild(document.createTextNode(pair));
 
            ele.appendChild(label);
            ele.appendChild(document.createElement("br"));
          }
        }
 
        var ele = document.getElementById("divUserRolesDelegation");
        ele.innerHTML = "";
        for (var option in optionArray2) {
          if (optionArray2.hasOwnProperty(option)) {
            //Replace option to to eliminate trailing zeros from bytes32 return value
            var pair = web3.utils.toAscii(optionArray2[option]).replace(/\u0000/g, '');
 
            var label = document.createElement('label')
            label.htmlFor = pair;
            label.appendChild(document.createTextNode(pair));
 
            ele.appendChild(label);
            ele.appendChild(document.createElement("br"));
          }
        }
      }
      else {
        alert('User does not exist!!');
      }
    }).catch(function (e) {
      console.log(e);
      alert('Some error occurred!!');
    });
  },
 
  checkUserPermission: function () {
    var user;
    Users.deployed().then(function (instance) {
      user = instance;
      return user.checkUser.call(userAddress);
    }).then(function (value) {
      if (value.valueOf() == false) {
        return Promise.reject('111');
      }
      return user.isUserAllowed.call(userAddress, usrPermission);
    }).then(function (value) {
      if (value.valueOf() == true) {
        document.getElementById('txtUserPermCheckResult').value = "True";
      } else {
        document.getElementById('txtUserPermCheckResult').value = "False";
      }
    }).catch(function (e) {
      if (e == "111") {
        alert('User dosen\'t exists!!');
      } else {
        console.log(e);
        alert('Some error occurred!!');
      }
    });
  },
 
  delegatePermission: function () {
    var self = this;
    var delegatorAddress = document.getElementById('txtUserDelegator').value;
    var beneficiaryAddress = document.getElementById('txtUserBeneficiary').value;
    var usrPermission = document.getElementById('txtUserPermRequested').value;
 
    var deleg;
    Delegation.deployed().then(function (instance) {
      deleg = instance;
      return deleg.getRoles.call();
    }).then(function (value) {
      if (value.valueOf() != "") {
        var valueArray = new Array();
        valueArray = value.valueOf();
        for(var loop in valueArray) {
          alert(web3.utils.toAscii(valueArray[loop]).replace(/\u0000/g, ''));
          return deleg.checkPermInRole(web3.utils.toAscii(valueArray[loop]).replace(/\u0000/g, ''),usrPermission);
        }
      } else {
        alert('No Roles to display!!');
      }
    }).then(function(result) {
      if (result.valueOf() == true) {
        console.log(web3.utils.toAscii(valueArray[loop]).replace(/\u0000/g, ''));
      }
    }).catch(function (e) {
      console.log(e);
      alert('Some error occurred!!');
    });
    
 
    /*var user;
    Users.deployed().then(function (instance) {
      user = instance;
      return user.getUserInfo.call(delegatorAddress);
    }).then(function (value) {
      let [optionArray1,optionArray2] = value.valueOf();
      if (optionArray1.length != 0) {
        for (var option in valueArray) {
          if (valueArray.hasOwnProperty(option)) {
            //Replace option to to eliminate trailing zeros from bytes32 return value
            var pair = web3.utils.toAscii(valueArray[option]).replace(/\u0000/g, '');
 
            var checkbox = document.createElement("input");
            checkbox.type = "checkbox";
            checkbox.name = "permValueUserModify[]";
            checkbox.value = pair;
            for (var chk in optionArray1) {
              if (web3.utils.toAscii(valueArray[option]).replace(/\u0000/g, '') == web3.utils.toAscii(optionArray1[chk]).replace(/\u0000/g, '')) {
                checkbox.checked = true;
              }
            }
            s2.appendChild(checkbox);
 
            var label = document.createElement('label')
            label.htmlFor = pair;
            label.appendChild(document.createTextNode(pair));
 
            s2.appendChild(label);
            s2.appendChild(document.createElement("br"));
          }
        }
      }
      else {
        alert('User does not exist!!');
      }
    }).catch(function (e) {
      console.log(e);
      alert('Some error occurred!!');
    });
  }*/

};

window.addEventListener('load', function () {
  //window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  window.web3 = new Web3(web3.currentProvider);

  App.start();
});
