pragma solidity ^0.4.17;

contract Role {
    //Structure definition to store various roles like IoT_Member.
    //It will store permissions associated to the role.
    struct UserRole {
        bytes32[] operation;
        uint index;
    }
  
    //Mapping to map role name to role information for the purpose of indexing
    mapping (bytes32 => UserRole) role;
    //Array to check the existance of role
    bytes32[] public chkRole;
    
    //Function to check the existance of role
    function checkRole(bytes32 _roleName) public view returns (bool) {
        if (chkRole.length == 0) {
            return false;
        }
        return (chkRole[role[_roleName].index] == _roleName);
    }
     
    //Function to add role
    function addRole(bytes32 _roleName, bytes32[] _permValue) public returns (bool) {
        UserRole memory temp;
        temp.operation = _permValue;
        temp.index = chkRole.push(_roleName)-1;  
        role[_roleName] = temp;
    
        //Successful addition of role
        return true;
    }
    
    //Function to delete role
    function deleteRole(bytes32 _roleName) public returns (bool) {
        uint rowToDelete = role[_roleName].index;
        bytes32 keyToMove = chkRole[chkRole.length-1];
        chkRole[rowToDelete] = keyToMove;
        role[keyToMove].index = rowToDelete; 
        chkRole.length--;
        
        //To delete datastructure
        delete role[_roleName];
        
        //Successful deletion of role
        return true;
    }
    
    //Function to modify role
    function modifyRole(bytes32 _roleName, bytes32[] _permValue) public returns (bool) {
        deleteRole(_roleName);
        addRole(_roleName, _permValue);
        return true;
    }
    
    //Function to send role information
    function getRoleInfo(bytes32 _roleName) public view returns (bytes32[]) {
        return (role[_roleName].operation);
    }

    //Function to send available roles
    function getRoles() public view returns (bytes32[]) {
        return (chkRole);
    }

    //Function to check the permission in role
    function checkPermInRole(bytes32 _roleName, bytes32 _perm) public view returns (bool) {
        for ( uint i = 0; i < role[_roleName].operation.length; i++ ) {
            if ( role[_roleName].operation[i] == _perm ) {
                return true;
            }
        }
        return false;
    }
}