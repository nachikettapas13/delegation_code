/*
Design consideration: instead of having array of node role pair inside one beneficiary user as per our original plan, we will have 
flat structure at beneficiary user id, node id and assigned role level. In this approach, the challange is to decide a unique identification 
so as to have efficient retrival.
Node-Role Tuple structure:
This approach will have efficient retrieval as beneficiary user id will be unique and will be assiciated with multiple role-tuple ids.
Problem:
The problem with this approach comes in the form of new delegation. To insert new node-role tuple, we need to first chech for the existance
of the user and if user exist, then insert new node-role tuple for the same user. If not create a new user with required data. Also, even if
the user exist, we need to iterate through all node-role tuples to be sure that user-node-role combination does not exist. Same is the case
with checking if the user-node-role exists. Thus, the iteration time will add-on to the search time which will be more compared to Flat structure
approach. Also, to track node-role tuple for efficient retrieval, we need to store redundant information in the form of a separate mapping.  
Flat Structure:
Easy to store information as you can have multiple new records for the same user as against node-role tuple approach where you 
have to first find the user structure and then insert node-role tupple in case user already exist(thus extra check is also required)
Problem:
With flat structure approach, we have the problem of efficient retrieval as mapping puts constraint on the key which cannot be structure
another mapping, or enum. This leads to making one of the entity out of beneficiary user id, node id and role assigned as key for the mapping.
But this will prevent further insert as combination of these entities will all map to same key leading to overwriting of the structure.
Hybrid Structure:
Only problem with flat structure was efficient retrieval as we could not exploit mapping. Solution to this can be to use keccak256 hash of 
combination of beneficiary, node and role. We will get a single unique hash which we can use for mapping the structure.
*/
pragma solidity ^0.4.17;

import "./Role.sol";

contract Delegation {

    address roleContractAddress;

    event CheckUserAccess(bytes32 usrName,bytes32 node,bytes32 perm,bool output);

    //Mandatory fields are beneficiaryUserID, nodeID and roleAssigned
    struct DelegationInfo {
        bytes32 delegatingUserID;
        bytes32 beneficiaryUserID;
        bytes32 nodeID;
        bytes32 roleAssigned;
        uint index;
    }

    
    mapping (bytes32 => DelegationInfo) delegInfo;
    
    bytes32[] chkDeleg;
    
    function Delegation(address _serviceAddress) public {
        roleContractAddress = _serviceAddress;
    }

    //Function checks tuple at beneficiary user id, node id and role level.
    function checkDelegation(bytes32 _usrBenifId, bytes32 _nodeId, bytes32 _roleAssigned) public view returns (bool) {
        if (chkDeleg.length == 0) {
            return false;
        }

        //Calculating the mapping key from beneficiary id, ndoe id and role
        bytes32 key = keccak256(_usrBenifId, _nodeId, _roleAssigned);

        //Searching 
        return (chkDeleg[delegInfo[key].index] == key);
    }
    
    //Function to insert delegation record. The validation of successful insertion can be done by checkDelegation function.
    //_parentDeleg = 0 implies genesis or level 1 delegation which is created by Admin
    function addDelegation(bytes32 _usrDelegId, bytes32 _usrBenifId, bytes32 _nodeId, bytes32 _roleAssigned) public {
        //Basic non empty check
        require(_usrBenifId[0] != 0 && _nodeId[0] != 0 && _roleAssigned[0] != 0);

        //Temperory structure initialization which is not stored in chain
        DelegationInfo memory tempDelegation;
        
        //Calculating the mapping key from beneficiary id, ndoe id and role
        bytes32 key = keccak256(_usrBenifId, _nodeId, _roleAssigned);

        //Populating values in the temporary delegation structure
        tempDelegation.beneficiaryUserID = _usrBenifId;
        tempDelegation.nodeID = _nodeId;
        tempDelegation.roleAssigned = _roleAssigned;
        
        //Check for genesis condition of the delegation. The design consideration is that we will have more cases of 
        //user to user delegation than initial genesis delegation and thus we can eliminate extra check by keeping that 
        //part of code in if
        if (_usrDelegId[0] != 0) {
            //Check to make sure that delegator indeed has access to the operation
            require(checkDelegation(_usrDelegId,_nodeId,_roleAssigned));
            tempDelegation.delegatingUserID = _usrDelegId;
        } else {
            //If it is level 1 delegation, then setting user id as "IoT_Admin"
            tempDelegation.delegatingUserID = "IoT_Admin";
        }

        //Save the index of delegation in the array
        tempDelegation.index = chkDeleg.push(key) - 1;
        //Save the delegation structure in the mapping
        delegInfo[key] = tempDelegation;
    }
    
    //Function to delete the delegation from the chain. Key which is combination of beneficiary id, node id and role
    //is used to delete the structure associated with it. The validation of successful insertion can be done by checkDelegation function.
    //Also we need to adjust the pointer to maintain delegation hierarchy.
    function deleteDelegation(bytes32 _usrBenifId, bytes32 _nodeId, bytes32 _roleAssigned) public {
        //Calculating the mapping key from beneficiary id, ndoe id and role
        bytes32 key = keccak256(_usrBenifId, _nodeId, _roleAssigned); 

        //Deletion logic
        uint rowToDelete = delegInfo[key].index;
        bytes32 keyToMove = chkDeleg[chkDeleg.length-1];
        chkDeleg[rowToDelete] = keyToMove;
        delegInfo[keyToMove].index = rowToDelete; 
        chkDeleg.length--;
        
        //To delete datastructure
        delete delegInfo[key];
    }
    
    //Function to check if a user has access on a specific node. The approach we followed is to get role for each user and if it has access
    //then check if beneficiary, node and role exist on the chain
    function isUserAllowed(bytes32 _usrBenifId, bytes32 _nodeId, bytes32 _access) public returns (bool) {
        Role _r = Role(roleContractAddress);
        
        for ( uint i = 0; i < chkDeleg.length; i++ ) {
            if (_r.checkPermInRole(delegInfo[chkDeleg[i]].roleAssigned,_access)) {
                //Calculating the mapping key from beneficiary id, ndoe id and role
                bytes32 key = keccak256(_usrBenifId, _nodeId, delegInfo[chkDeleg[i]].roleAssigned);

                if (chkDeleg[delegInfo[key].index] == key) {
                    CheckUserAccess(_usrBenifId, _nodeId, _access, true);
                    return true;
                }
            }
        }
        CheckUserAccess(_usrBenifId, _nodeId, _access, false);
        return false;
    }

    //Function to check the information of the delegation struncture to verify the correct working of the code
    function getDelegationInfo(bytes32 _usrBenifId, bytes32 _nodeId, bytes32 _roleAssigned) public view returns (bytes32) {
        //Calculating the mapping key from beneficiary id, ndoe id and role
        bytes32 key = keccak256(_usrBenifId, _nodeId, _roleAssigned);

        //Returning delegator id and parent delegation
        return (delegInfo[key].delegatingUserID);
    }
}