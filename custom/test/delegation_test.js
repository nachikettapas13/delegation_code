var Delegation = artifacts.require("./Delegation.sol");

contract('delegation_test', function(accounts) {
  it("should assert true", function() {
    var deleg;
    Delegation.deployed().then(function(instance) {
      deleg = instance;
      console.log("Address is : " + deleg.address);
      deleg.addRole('IoT_M',['Create']);
      return deleg.checkRole.call("IoT_M");
    }).then(function(result) {
      console.log("Result " + result.valueOf());
    })
  });
});
